package com.cdac.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Controller
public class PageController {

	@RequestMapping("/dashboard")
	public String showDashboardPage(HttpServletRequest request) {
		ServletContext servletContext = request.getServletContext();

		System.out.println(servletContext);

		/*
		 * Output :
		 * 
		 * org.apache.catalina.core.ApplicationContextFacade@786387a6
		 */

// ============================================================================================ //

		/*
		 * findWebApplicationContext(ServletContext servletContext);
		 * 
		 * => Objective : Find a unique WebApplicationContext for this web application :
		 * either the root web app context (preferred) or a unique
		 * WebApplicationContextamong the registered ServletContext attributes
		 * (typically coming from a single DispatcherServlet in the current web
		 * application).
		 * 
		 * => Returns : the desired WebApplicationContext for this web application, or
		 * null if none
		 */
		WebApplicationContext servletWebApplicationContext = WebApplicationContextUtils
				.findWebApplicationContext(servletContext);
		System.out.println(servletWebApplicationContext);
		/*
		 * Output :
		 * 
		 * Root WebApplicationContext, started on Fri May 05 18:55:32 IST 2023
		 */
// ============================================================================================ //

		/*
		 * getWebApplicationContext(ServletContext servletContext);
		 * 
		 * => Objective : Find the root WebApplicationContext for this web app,
		 * typically loaded via org.springframework.web.context.ContextLoaderListener.
		 * 
		 * => Returns:the root WebApplicationContext for this web application, or null
		 * if none
		 */
		WebApplicationContext rootWebApplicationContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		System.out.println(rootWebApplicationContext);
		/*
		 * Output :
		 * 
		 * Root WebApplicationContext, started on Fri May 05 18:55:32 IST 2023
		 */

// ============================================================================================ //

		/*
		 * getRequiredWebApplicationContext(ServletContext serveltContext)
		 * 
		 * => Objective : Find the root WebApplicationContext for this web app,
		 * typically loaded via org.springframework.web.context.ContextLoaderListener.
		 * 
		 * => Returns:the root WebApplicationContext for this web app
		 * 
		 * => Throws:IllegalStateException - if the root WebApplicationContext could not
		 * be found
		 */
		WebApplicationContext rootWebApplicationContext_ = WebApplicationContextUtils
				.getRequiredWebApplicationContext(servletContext);
		System.out.println(rootWebApplicationContext_);
		/*
		 * Output :
		 * 
		 * Root WebApplicationContext, started on Fri May 05 18:55:32 IST 2023
		 */
// ============================================================================================ //

		return "dashboard";
	}

}